##Requirements
###Environment
- Python 3.4.3
- Django 1.10.4

###Pre-setup
#####PostgreSQL
- Read: [Installing postreSQL in MacOS](https://mjanja.ch/2016/04/using-homebrews-postgresql-mac-os-x/)
- prepare and create database with the name `tpspilkada2017`
- create a **user** with a **password** and grant it to `tpspilkada2017`

#####Required Python Packages
- `pip install Django`
- `pip install psycopg2`
- `pip install Pillow`

##Installing
- Clone this repo
- Configure **settings_local.py** 

	```
	DATABASES = {
	    'default': {
	        'ENGINE': 'django.db.backends.postgresql',
	        'NAME': 'tpspilkada2017',
	        'USER': '', # <~ fill with configured user for tpspilkada2017
	        'PASSWORD': '' # <~ fill with configured password of user for tpspilkada2017
	    }
	}
	```

- run: `./manage.py migrate`
- create a superuser `./manage.py createsuperuser`
- Install the **KPU** data from `kpu.sql` into `tpspilkada2017`, note it have no `Pemilih` data in it!

##Up and Running in Development
- `./manage.py runserver`
- open in web browser `http://127.0.0.1:8000`

## Resources
- [Learn Python - Interactive](http://learnpython.org)
- [Getting Started with Django ](https://www.djangoproject.com/start/)

## Tips
- `brew install pyenv`
- Use: [VirtualEnv](https://github.com/yyuu/pyenv-virtualenv)


## Coding Standard | Style Guide
- Follow PEP 8 - For details see: [https://www.python.org/dev/peps/pep-0008/](https://www.python.org/dev/peps/pep-0008/)
- Pleaase Ignore Maximum *Code* Line Length of 79!
