DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tpspilkada2017',
        'USER': 'benny',
        'PASSWORD': 'benny'
    }
}

WSGI_APPLICATION = 'tpspilkada2017.local.wsgi.application'

SITE_IDS = {
    'tpspilkada2017.local': 1
}

SITE_ID = SITE_IDS['tpspilkada2017.local']
MEDIA_URL = 'http://cdn.tpspilkada2017.local/media/'
STATIC_URL = 'http://cdn.tpspilkada2017.local/static/'
ADMIN_MEDIA_PREFIX = 'http://cdn.tpspilkada2017.local/static/admin/'
