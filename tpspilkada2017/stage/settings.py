ALLOWED_HOSTS = ['p-homepage-demo-01', '10.240.8.200', '127.0.0.1']

INTERNAL_IPS = ['127.0.0.1', '10.240.8.200']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tpspilkada2017',
        'USER': 'benny',
        'PASSWORD': 'benny'
    }
}

WSGI_APPLICATION = 'tpspilkada2017.stage.wsgi.application'

SITE_ID = 2
MEDIA_URL = 'http://10.240.8.200:8000/media/'
STATIC_URL = 'http://10.240.8.200:8000/static/'
ADMIN_MEDIA_PREFIX = 'http://10.240.8.200:8000/static/admin/'
