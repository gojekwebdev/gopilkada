import os
import sys
import socket

PROJECT_DIR = os.path.dirname(__file__)
BASE_DIR = os.path.dirname(PROJECT_DIR)

VENDOR_DIR = os.path.join(BASE_DIR, 'vendor')
APPS_DIR = os.path.join(BASE_DIR, 'apps')

if VENDOR_DIR not in sys.path:
    sys.path.insert(0, VENDOR_DIR)
if APPS_DIR not in sys.path:
    sys.path.insert(0, APPS_DIR)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '(#wrs8vl_!*@zc^03q@0lwk7_a8na-_-%-=q!h!80g)xh!67mj'

HOSTNAME = socket.gethostname()
PRODUCTION_SERVERS = []  # @TODO: add production server hostname here
STAGE_SERVERS = ['p-homepage-demo-01']

LIVEHOST = HOSTNAME in PRODUCTION_SERVERS
STAGEHOST = HOSTNAME in STAGE_SERVERS

LOCALHOST = not LIVEHOST and not STAGEHOST

DEBUG = not LIVEHOST

STATIC_ROOT = os.path.join(BASE_DIR, 'cdn', 'static/')
MEDIA_ROOT = os.path.join(BASE_DIR, 'cdn', 'media/')

INSTALLED_APPS = [
    'suit',
    'suit_redactor',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'web',
    'pemilih'
]

if LOCALHOST and DEBUG:
    INSTALLED_APPS += ['debug_toolbar', 'django_extensions']

MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware'
]

ROOT_URLCONF = 'tpspilkada2017.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
            ],
        },
    },
]

AUTH_PASSWORD_VALIDATORS = [
    # {
    #     'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    # },
]


LANGUAGE_CODE = 'id'
TIME_ZONE = 'Asia/Jakarta'
USE_I18N = True
USE_L10N = True
USE_TZ = True


SUIT_CONFIG = {
    'ADMIN_NAME': 'TPS Pilkada 2017',
    'LIST_PER_PAGE': 50
}

if LIVEHOST:
    from .live.settings import *
elif STAGEHOST:
    from .stage.settings import *
else:
    from .local.settings import *

