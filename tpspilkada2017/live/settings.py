ALLOWED_HOSTS = []  # TODO: configure the hostname of

INTERNAL_IPS = ['127.0.0.1']  # TODO: configure inernal ip

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tpspilkada2017',
        'USER': '',
        'PASSWORD': ''
    }
}

WSGI_APPLICATION = 'tpspilkada2017.live.wsgi.application'

SITE_ID = 3  # TODO: configure the correct ones
MEDIA_URL = ''  # TODO: configure the correct ones
STATIC_URL = ''  # TODO: configure the correct ones
ADMIN_MEDIA_PREFIX = ''  # TODO: configure the correct ones
