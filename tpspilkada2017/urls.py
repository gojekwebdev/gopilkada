from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from web import views
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^kecamatan$', views.HomeView.as_view(), name='get_kecamatan'),
    url(r'^kelurahan$', views.HomeView.as_view(), name='get_kelurahan'),
    url(r'^tps$', views.HomeView.as_view(), name='get_tps'),
    # url(r'^tps/(?P<id_kelurahan>\d+)$', views.HomeView.as_view(), name='get_tps'),
    url(r'^invalid-request$', TemplateView.as_view(template_name='handler/invalid_request.html'), name='invalid_request'),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

if settings.LOCALHOST:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
