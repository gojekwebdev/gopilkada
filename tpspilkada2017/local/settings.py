import os

ALLOWED_HOSTS = ['tpspilkada2017.local', '127.0.0.1']

INTERNAL_IPS = ['127.0.0.1']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tpspilkada2017',
        'USER': 'benny',
        'PASSWORD': 'benny'
    }
}

WSGI_APPLICATION = 'tpspilkada2017.local.wsgi.application'

SITE_IDS = {
    'tpspilkada2017.local': 1
}

STATIC_ROOT = ''
MEDIA_ROOT = ''

SITE_ID = SITE_IDS['tpspilkada2017.local']
MEDIA_URL = '/media/'
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'

STATICFILES_DIRS = (os.path.join('cdn', 'static'), os.path.join('cdn', 'media'))
