import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tpspilkada2017.settings")

PROJECT_DIR = os.path.dirname(__file__)
BASE_DIR = os.path.dirname(PROJECT_DIR)

VENDOR_DIR = os.path.join(BASE_DIR, 'vendor')
APPS_DIR = os.path.join(BASE_DIR, 'apps')

sys.path.append('/Users/benny/.pyenv/versions/django1.10.4/lib/python3.4/site-packages')

if VENDOR_DIR not in sys.path:
    sys.path.insert(0, VENDOR_DIR)
if APPS_DIR not in sys.path:
    sys.path.insert(0, APPS_DIR)
if BASE_DIR not in sys.path:
    sys.path.append(BASE_DIR)

os.environ["DJANGO_SETTINGS_MODULE"] = "tpspilkada2017.settings"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
