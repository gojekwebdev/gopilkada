from django.views.generic import TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from pemilih import models as pm
from django.core import serializers
from django.urls import reverse


class HomeView(TemplateView):
    template_name = 'home.html'
    size = 1000, 1000

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update(dict(self.request.POST.items()))

        if not request.is_ajax():
            return HttpResponseRedirect(reverse('invalid_request'))

        if 'id_kabkota' in context:
            kecamatan = pm.Kecamatan.objects.filter(kabkota__id=context['id_kabkota'])
            kecamatan_json = serializers.serialize("json", kecamatan, fields=('namaKecamatan',))
            return HttpResponse(kecamatan_json, content_type='json')

        if 'id_kecamatan' in context:
            kelurahan = pm.Kelurahan.objects.filter(kecamatan__id=context['id_kecamatan'])
            kelurahan_json = serializers.serialize("json", kelurahan, fields=('namaKelurahan',))
            return HttpResponse(kelurahan_json, content_type='json')

        if 'nama' in context and 'id_kelurahan' in context:
            try:
                pemilih = pm.Pemilih.objects.get(nama=context['nama'].upper(), kelurahan__id=context['id_kelurahan'])
                pemilih_json = serializers.serialize("json", [pemilih.tps], fields=('no',))[1:-1]
            except Exception as e:
                print(e)
                pemilih_json = '{}'

            return HttpResponse(pemilih_json, content_type='json')

        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        context['kabkota'] = pm.KabKota.objects.filter(propinsi__namaWilayah__icontains='jakarta')

        return context
