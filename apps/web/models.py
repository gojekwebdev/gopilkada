from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site as BaseSite


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, verbose_name='created at')
    updated = models.DateTimeField(auto_now=True, verbose_name='updated at')

    created_by = models.ForeignKey(User, related_name='%(app_label)s_%(class)s_created_by', verbose_name='created by', editable=False)
    updated_by = models.ForeignKey(User, related_name='%(app_label)s_%(class)s_updated_by', verbose_name='updated by', editable=False)

    class Meta:
        abstract = True


class SiteAlias(BaseModel):
    name = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Site(BaseSite, BaseModel):
    is_product = models.BooleanField(default=True)
    icon = models.ImageField(upload_to='site_icon', blank=True, null=True)
    users = models.ManyToManyField(User, related_name='%(app_label)s_%(class)s_site_user')
    alias = models.ManyToManyField(SiteAlias, blank=True)

    def __str__(self):
        return self.name


class Banner(BaseModel):
    title = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='banner')

    def __str__(self):
        return self.title


class Picture(BaseModel):
    title = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='picture')

    def __str__(self):
        return self.title


class Category(BaseModel):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name


class Tag(BaseModel):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Post(BaseModel):
    title = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(max_length=512, blank=True, null=True)
    article = models.TextField()
    excerpt = models.TextField(blank=True, null=True, verbose_name='preface')
    published_at = models.DateTimeField()

    featured = models.BooleanField(default=False)

    categories = models.ManyToManyField(Category, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)

    view = models.IntegerField(default=0)

    site = models.ForeignKey(Site)

    active = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class PostPicture(BaseModel):
    picture = models.ForeignKey(Picture)
    as_cover = models.BooleanField(default=False)
    post = models.ForeignKey(Post)
    order = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return str(self.id)


class PostBanner(BaseModel):
    banner = models.ForeignKey(Banner)
    post = models.ForeignKey(Post)
    order = models.PositiveSmallIntegerField(default=1)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return str(self.id)


class FaqSection(BaseModel):
    name = models.CharField(max_length=255)
    site = models.ForeignKey(Site)
    order = models.PositiveSmallIntegerField(default=1)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.name


class Faq(BaseModel):
    section = models.ForeignKey(FaqSection)
    question = models.CharField(max_length=200)
    answer = models.TextField()
    order = models.PositiveSmallIntegerField(default=1)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.question


class SocialMedia(BaseModel):
    title = models.CharField(max_length=100, unique=True)
    icon = models.ImageField(upload_to='social_media')
    url = models.URLField()
    order = models.PositiveSmallIntegerField(default=1)

    site = models.ForeignKey(Site)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.title





