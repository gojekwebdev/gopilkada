from django.forms import ModelForm
from django.contrib import admin
from suit_redactor.widgets import RedactorWidget
from web import models as web_models


class BaseAdmin(admin.ModelAdmin):
    readonly_fields = ['created', 'created_by', 'updated', 'updated_by']

    def save_model(self, request, instance, form, change):
        instance.created_by = request.user
        if not instance.pk:
            instance.updated_by = request.user
            # instance.checked = True

        instance.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            if isinstance(instance, web_models.BaseModel):
                if not instance.created_by_id:
                    instance.created_by = request.user

                instance.updated_by = request.user
                instance.save()


class PostForm(ModelForm):
    class Meta:
        widgets = {
            'article': RedactorWidget(editor_options={'lang': 'en'})
        }


class PostPictureInline(admin.StackedInline):
    model = web_models.PostPicture
    extra = 0


class PostBannerInline(admin.StackedInline):
    model = web_models.PostBanner
    extra = 0


class PostAdmin(BaseAdmin):
    form = PostForm
    list_display = ['title', 'published_at']
    inlines = [PostPictureInline, PostBannerInline]


admin.site.register(web_models.SiteAlias, BaseAdmin)
admin.site.register(web_models.Site, BaseAdmin)
admin.site.register(web_models.Banner, BaseAdmin)
admin.site.register(web_models.Picture, BaseAdmin)
admin.site.register(web_models.Category, BaseAdmin)
admin.site.register(web_models.Tag, BaseAdmin)
admin.site.register(web_models.Post, PostAdmin)
admin.site.register(web_models.PostPicture, BaseAdmin)
admin.site.register(web_models.PostBanner, BaseAdmin)
admin.site.register(web_models.FaqSection, BaseAdmin)
admin.site.register(web_models.Faq, BaseAdmin)
admin.site.register(web_models.SocialMedia, BaseAdmin)
