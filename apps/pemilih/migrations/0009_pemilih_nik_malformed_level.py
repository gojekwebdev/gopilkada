# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-13 06:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pemilih', '0008_pemilih_nik_lengkap'),
    ]

    operations = [
        migrations.AddField(
            model_name='pemilih',
            name='nik_malformed_level',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(1, '123456******123*'), (2, '123456**********'), (2, '1234************'), (3, '1***************')], null=True),
        ),
    ]
