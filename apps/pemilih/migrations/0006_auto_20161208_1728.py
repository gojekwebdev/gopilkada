# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-12-08 10:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pemilih', '0005_auto_20161206_1213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pemilih',
            name='tempatLahir',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='tempat lahir'),
        ),
    ]
