from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from tpspilkada2017 import settings
from pemilih import models as pm
import csv
import os
import re
import datetime
import requests
import json
import pprint


class Command(BaseCommand):
    help = "Fetch JSON from KPU.GO.ID for Pilkada 2017 -- (Data Pemiih & TPS)\
        Available Sub-Commands: [store_propinsi, store_kabkota, store_kecamatan, store_kelurahan, store_tps, store_pemilih]"
    start_url = 'https://pilkada2017.kpu.go.id/pemilih/dps/listNasional.json'
    fetch_propinsi = ['DKI JAKARTA']
    logs = {
        'info': [],
        'error': []
    }

    def add_arguments(self, parser):
        parser.add_argument('sub-command', nargs='+', type=str)

    # pp = pprint.PrettyPrinter(indent=2)

    def store_propinsi(self):
        try:
            r = requests.get(self.start_url)
            d = json.loads(r.text)
            list_propinsi = d['aaData']
        except Exception as e:
            print(e)
            self.logs['error'].append('Failed when storing a propinsi -- url="{}"'.format(self.start_url))

        for item in list_propinsi:
            propinsi, created = pm.Propinsi.objects.get_or_create(
                namaWilayah=item['namaWilayah'],

                jmlTps=item['jmlTps'],

                jmlPemilihLaki=item['jmlPemilihLaki'],
                jmlPemilihPerempuan=item['jmlPemilihPerempuan'],

                jmlPemilihPemulaLaki=item['jmlPemilihPemulaLaki'],
                jmlPemilihPemulaPerempuan=item['jmlPemilihPemulaPerempuan'],

                jmlPemilihKosong=item['jmlPemilihKosong'],

                jmlDifabel1=item['jmlDifabel1'],
                jmlDifabel2=item['jmlDifabel2'],
                jmlDifabel3=item['jmlDifabel3'],
                jmlDifabel4=item['jmlDifabel4'],
                jmlDifabel5=item['jmlDifabel5'],

                totalPemilihPemula=item['totalPemilihPemula'],
                totalDifabel=item['totalDifabel'],
                totalPemilih=item['totalPemilih'],

                persenPemilihPemula=item['persenPemilihPemula'],
                persenDifabel=item['persenDifabel'],

            )
            if created:
                info = 'Menambahakan propinsi "{}"'.format(item['namaWilayah'])
            else:
                info = 'Data sudah ada: propinsi "{}"'.format(item['namaWilayah'])
            print(info)
            self.logs['info'].append(info)

    def store_kabkota(self):
        list_propinsi = pm.Propinsi.objects.filter(namaWilayah__in=self.fetch_propinsi)

        for propinsi in list_propinsi:
            self.start_url = "https://pilkada2017.kpu.go.id/pemilih/dps/{}/listDps.json".format(propinsi.namaWilayah)

            try:
                r = requests.get(self.start_url)
                d = json.loads(r.text)
                list_kabkota = d['aaData']
            except Exception as e:
                print(e)
                self.logs['error'].append('Failed when storing a kabkota -- url="{}"'.format(self.start_url))
                continue

            for item in list_kabkota:
                kabkota, created = pm.KabKota.objects.get_or_create(
                    namaKabKota=item['namaKabKota'],
                    propinsi=propinsi,

                    jmlTps=item['jmlTps'],

                    jmlPemilihLaki=item['jmlPemilihLaki'],
                    jmlPemilihPerempuan=item['jmlPemilihPerempuan'],

                    jmlPemilihPemulaLaki=item['jmlPemilihPemulaLaki'],
                    jmlPemilihPemulaPerempuan=item['jmlPemilihPemulaPerempuan'],

                    jmlPemilihKosong=item['jmlPemilihKosong'],

                    jmlDifabel1=item['jmlDifabel1'],
                    jmlDifabel2=item['jmlDifabel2'],
                    jmlDifabel3=item['jmlDifabel3'],
                    jmlDifabel4=item['jmlDifabel4'],
                    jmlDifabel5=item['jmlDifabel5'],

                    totalPemilihPemula=item['totalPemilihPemula'],
                    totalDifabel=item['totalDifabel'],
                    totalPemilih=item['totalPemilih'],

                    persenPemilihPemula=item['persenPemilihPemula'],
                    persenDifabel=item['persenDifabel'],
                )
                if created:
                    info = 'Menambahakan kabkota "{}"'.format(item['namaKabKota'])
                else:
                    info = 'Data sudah ada: kabkota "{}"'.format(item['namaKabKota'])
                print(info)
                self.logs['info'].append(info)

    def store_kecamatan(self):
        kabkota_list = pm.KabKota.objects.filter(propinsi__namaWilayah__in=self.fetch_propinsi)

        for kabkota in kabkota_list:
            self.start_url = "https://pilkada2017.kpu.go.id/pemilih/dps/{}/{}/listDps.json".format(kabkota.propinsi.namaWilayah, kabkota.namaKabKota)

            try:
                r = requests.get(self.start_url)
                d = json.loads(r.text)
                list_kecamatan = d['aaData']
            except Exception as e:
                print(e)
                self.logs['error'].append('Failed when storing a kecamatan -- url="{}"'.format(self.start_url))
                continue

            for item in list_kecamatan:
                kecamatan, created = pm.Kecamatan.objects.get_or_create(
                    namaKecamatan=item['namaKecamatan'],
                    propinsi=kabkota.propinsi,
                    kabkota=kabkota,

                    jmlTps=item['jmlTps'],

                    jmlPemilihLaki=item['jmlPemilihLaki'],
                    jmlPemilihPerempuan=item['jmlPemilihPerempuan'],

                    jmlPemilihPemulaLaki=item['jmlPemilihPemulaLaki'],
                    jmlPemilihPemulaPerempuan=item['jmlPemilihPemulaPerempuan'],

                    jmlPemilihKosong=item['jmlPemilihKosong'],

                    jmlDifabel1=item['jmlDifabel1'],
                    jmlDifabel2=item['jmlDifabel2'],
                    jmlDifabel3=item['jmlDifabel3'],
                    jmlDifabel4=item['jmlDifabel4'],
                    jmlDifabel5=item['jmlDifabel5'],

                    totalPemilihPemula=item['totalPemilihPemula'],
                    totalDifabel=item['totalDifabel'],
                    totalPemilih=item['totalPemilih'],

                    persenPemilihPemula=item['persenPemilihPemula'],
                    persenDifabel=item['persenDifabel'],
                )
                if created:
                    info = 'Menambahakan kecamatan "{}"'.format(item['namaKecamatan'])
                else:
                    info = 'Data sudah ada: kecamatan "{}"'.format(item['namaKecamatan'])
                print(info)
                self.logs['info'].append(info)

    def store_kelurahan(self):
        kecamatan_list = pm.Kecamatan.objects.filter(propinsi__namaWilayah__in=self.fetch_propinsi)

        for kecamatan in kecamatan_list:
            self.start_url = "https://pilkada2017.kpu.go.id/pemilih/dps/{}/{}/{}/listDps.json".format(kecamatan.propinsi.namaWilayah, kecamatan.kabkota.namaKabKota, kecamatan.namaKecamatan)

            try:
                r = requests.get(self.start_url)
                d = json.loads(r.text)
                list_kelurahan = d['aaData']
            except Exception as e:
                print(e)
                self.logs['error'].append('Failed when storing a kelurahan -- url="{}"'.format(self.start_url))
                continue

            for item in list_kelurahan:
                kelurahan, created = pm.Kelurahan.objects.get_or_create(
                    namaKelurahan=item['namaKelurahan'],
                    propinsi=kecamatan.propinsi,
                    kabkota=kecamatan.kabkota,
                    kecamatan=kecamatan,

                    jmlTps=item['jmlTps'],

                    jmlPemilihLaki=item['jmlPemilihLaki'],
                    jmlPemilihPerempuan=item['jmlPemilihPerempuan'],

                    jmlPemilihPemulaLaki=item['jmlPemilihPemulaLaki'],
                    jmlPemilihPemulaPerempuan=item['jmlPemilihPemulaPerempuan'],

                    jmlPemilihKosong=item['jmlPemilihKosong'],

                    jmlDifabel1=item['jmlDifabel1'],
                    jmlDifabel2=item['jmlDifabel2'],
                    jmlDifabel3=item['jmlDifabel3'],
                    jmlDifabel4=item['jmlDifabel4'],
                    jmlDifabel5=item['jmlDifabel5'],

                    totalPemilihPemula=item['totalPemilihPemula'],
                    totalDifabel=item['totalDifabel'],
                    totalPemilih=item['totalPemilih'],

                    persenPemilihPemula=item['persenPemilihPemula'],
                    persenDifabel=item['persenDifabel'],
                )
                if created:
                    info = 'Menambahakan kelurahan "{}"'.format(item['namaKelurahan'])
                else:
                    info = 'Data sudah ada: kelurahan "{}"'.format(item['namaKelurahan'])
                print(info)
                self.logs['info'].append(info)

    def store_tps(self):
        kelurahan_list = pm.Kelurahan.objects.filter(propinsi__namaWilayah__in=self.fetch_propinsi)

        for kelurahan in kelurahan_list:
            self.start_url = "https://pilkada2017.kpu.go.id/pemilih/dps/{}/{}/{}/{}/listDps.json".format(kelurahan.propinsi.namaWilayah, kelurahan.kabkota.namaKabKota, kelurahan.kecamatan.namaKecamatan, kelurahan.namaKelurahan)

            try:
                r = requests.get(self.start_url)
                d = json.loads(r.text)
                list_tps = d['aaData']
            except Exception as e:
                print(e)
                self.logs['error'].append('Failed when storing a tps -- url="{}"'.format(self.start_url))
                continue

            for item in list_tps:
                tps, created = pm.Tps.objects.get_or_create(
                    no=item['tps'],

                    propinsi=kelurahan.propinsi,
                    kabkota=kelurahan.kabkota,
                    kecamatan=kelurahan.kecamatan,
                    kelurahan=kelurahan,

                    jmlPemilihLaki=item['jmlPemilihLaki'],
                    jmlPemilihPerempuan=item['jmlPemilihPerempuan'],

                    jmlPemilihPemulaLaki=item['jmlPemilihPemulaLaki'],
                    jmlPemilihPemulaPerempuan=item['jmlPemilihPemulaPerempuan'],

                    jmlPemilihKosong=item['jmlPemilihKosong'],

                    jmlDifabel1=item['jmlDifabel1'],
                    jmlDifabel2=item['jmlDifabel2'],
                    jmlDifabel3=item['jmlDifabel3'],
                    jmlDifabel4=item['jmlDifabel4'],
                    jmlDifabel5=item['jmlDifabel5'],

                    totalPemilihPemula=item['totalPemilihPemula'],
                    totalDifabel=item['totalDifabel'],
                    totalPemilih=item['totalPemilih'],

                    persenPemilihPemula=item['persenPemilihPemula'],
                    persenDifabel=item['persenDifabel'],
                )
                if created:
                    info = 'Menambahakan tps "{}" untuk kelurahan "{}"'.format(item['tps'], kelurahan.namaKelurahan)
                else:
                    info = 'Data sudah ada: tps "{}" untuk kelurahan "{}"'.format(item['tps'], kelurahan.namaKelurahan)
                print(info)
                self.logs['info'].append(info)

    def store_pemilih(self):
        tps_list = pm.Tps.objects.filter(propinsi__namaWilayah__in=self.fetch_propinsi)

        for tps in tps_list:
            self.start_url = "https://pilkada2017.kpu.go.id/pemilih/dps/{}/{}/{}/{}/{}/listDps.json".format(tps.propinsi.namaWilayah, tps.kabkota.namaKabKota, tps.kecamatan.namaKecamatan, tps.kelurahan.namaKelurahan, tps.no)

            try:
                r = requests.get(self.start_url)
                d = json.loads(r.text)
                list_tps = d['data']
            except Exception as e:
                print(e)
                self.logs['error'].append('Failed when storing a kelurahan -- url="{}"'.format(self.start_url))
                continue

            for item in list_tps:
                tanggalLahir = datetime.datetime.strptime(item['tanggalLahir'], '%b %d, %Y %I:%M:%S %p').date()
                if item['jenisKelamin'] == 'Laki-Laki':
                    jenisKelamin = 1
                else:
                    jenisKelamin = 2
                pemilih, created = pm.Pemilih.objects.get_or_create(
                    nama=item['nama'],
                    id_pemilih=item['id'],
                    nik=item['nik'],
                    ektp=item['ektp'],

                    tps=tps,

                    propinsi=tps.propinsi,
                    kabkota=tps.kabkota,
                    kecamatan=tps.kecamatan,
                    kelurahan=tps.kelurahan,
                    idWilayah=item['idWilayah'],

                    tempatLahir=item['tempatLahir'],
                    tanggalLahir=tanggalLahir,
                    jenisKelamin=jenisKelamin,
                    cacat=item['cacat'],
                    kawin=item['kawin'],
                )
                if created:
                    info = 'Menambahakan pemilih "{}" di Tps {} untuk kelurahan "{}"'.format(item['nama'], item['tps'], tps.kelurahan.namaKelurahan)
                else:
                    info = 'Data sudah ada: pemilih "{}" di Tps {} untuk kelurahan "{}"'.format(item['nama'], item['tps'], tps.kelurahan.namaKelurahan)
                print(info)
                self.logs['info'].append(info)

    def handle(self, *args, **options):
        try:
            eval("self.{}()".format(options['sub-command'][0]))
        except:
            print('Invalid sub-command')
            print(self.help)

