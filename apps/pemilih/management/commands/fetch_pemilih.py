from django.core.management.base import BaseCommand
from pemilih import models as pm
import datetime
import requests
import subprocess
import os


class Command(BaseCommand):
    help = 'Fetch JSON from KPU.GO.ID for Pilkada 2017 -- (Data Pemiih & TPS)'
    start_url = 'https://pilkada2017.kpu.go.id/pemilih/dps/listNasional.json'
    fetch_propinsi = ['DKI JAKARTA']
    logs = {
        'info': [],
        'error': []
    }

    STORAGE = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))), 'cdn', 'media', 'pemilih')

    def add_arguments(self, parser):
        parser.add_argument('id', nargs='+', type=int)

    def store_pemilih(self, tps_id):
        try:
            tps = pm.Tps.objects.get(id=tps_id)
        except:
            print('TPS dengan id={} tidak ditemukan'.format(tps_id))
            exit(1)
        self.start_url = "https://pilkada2017.kpu.go.id/pemilih/dps/{}/{}/{}/{}/{}/listDps.json".format(tps.propinsi.namaWilayah, tps.kabkota.namaKabKota, tps.kecamatan.namaKecamatan, tps.kelurahan.namaKelurahan, tps.no)

        TARGET = os.path.join(self.STORAGE, "{}".format(tps.propinsi.id), "{}".format(tps.kabkota.id), "{}".format(tps.kecamatan.id), "{}".format(tps.kelurahan.id))
        subprocess.call(['mkdir', '-p', TARGET])
        filename = os.path.join(TARGET, "{}.json".format(tps.no))

        if os.path.exists(filename) and os.path.getsize(filename) > 20:
            print('File {} exists! - returning'.format(filename))
            return

        print('Requesting {}'.format(self.start_url))

        try:
            r = requests.get(self.start_url)
        except Exception as e:
            print(e)
            print('Failed when storing a pemilih -- url="{}"'.format(self.start_url))
            exit(1)

        print('Writting to {}'.format(filename))

        with open(filename, 'w') as f:
            f.write(r.text)
        f.close()

        # # dump all json into {tps.id}.json

        # for item in list_pemilih:
        #     tanggalLahir = datetime.datetime.strptime(item['tanggalLahir'], '%b %d, %Y %I:%M:%S %p').date()
        #     if item['jenisKelamin'] == 'Laki-Laki':
        #         jenisKelamin = 1
        #     else:
        #         jenisKelamin = 2
        #     pemilih, created = pm.Pemilih.objects.get_or_create(
        #         nama=item['nama'],
        #         id_pemilih=item['id'],
        #         nik=item['nik'],
        #         ektp=item['ektp'],

        #         tps=tps,

        #         propinsi=tps.propinsi,
        #         kabkota=tps.kabkota,
        #         kecamatan=tps.kecamatan,
        #         kelurahan=tps.kelurahan,
        #         idWilayah=item['idWilayah'],

        #         tempatLahir=item['tempatLahir'],
        #         tanggalLahir=tanggalLahir,
        #         jenisKelamin=jenisKelamin,
        #         cacat=item['cacat'],
        #         kawin=item['kawin'],
        #     )
        #     if created:
        #         info = 'Menambahakan pemilih "{}" di Tps {} untuk kelurahan "{}"'.format(item['nama'], item['tps'], tps.kelurahan.namaKelurahan)
        #     else:
        #         info = 'Data sudah ada: pemilih "{}" di Tps {} untuk kelurahan "{}"'.format(item['nama'], item['tps'], tps.kelurahan.namaKelurahan)
        #     print(info)
        #     self.logs['info'].append(info)

    def handle(self, *args, **options):
        self.store_pemilih(options['id'][0])

