from django.core.management.base import BaseCommand
from pemilih import models as pm
import datetime
import requests
import json
import os
import subprocess


class Command(BaseCommand):
    help = 'Fetch JSON from KPU.GO.ID for Pilkada 2017 -- (Data Pemiih & TPS)'
    start_url = 'https://pilkada2017.kpu.go.id/pemilih/dps/listNasional.json'
    fetch_propinsi = ['DKI JAKARTA']
    logs = {
        'info': [],
        'error': []
    }

    MANAGEMENT_CMD = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))), 'manage.py')

    def add_arguments(self, parser):
        parser.add_argument('n sub-process', nargs='+', type=int)

    def store_kecamatan(self, n_subprocesses):
        kabkota_list = pm.KabKota.objects.filter(propinsi__namaWilayah__in=self.fetch_propinsi)

        n_kabkota = kabkota_list.count()
        for i in range(n_kabkota):
            subprocesses = []
            nsub = n_subprocesses
            idx = i
            while nsub > 0 and idx < (n_kabkota - 1):
                subprocesses.append(subprocess.Popen([self.MANAGEMENT_CMD, 'fetch_kecamatan', "{}".format(kabkota_list[idx].id)]))
                nsub -= 1
                idx += 1

            for j in range(len(subprocesses)):
                subprocesses[j].wait()

    def handle(self, *args, **options):
        self.store_kecamatan(options['n sub-process'][0])
