from django.core.management.base import BaseCommand
from pemilih import models as pm
import datetime
import requests
import json


class Command(BaseCommand):
    help = 'Fetch JSON from KPU.GO.ID for Pilkada 2017 -- (Data Pemiih & TPS)'
    start_url = 'https://pilkada2017.kpu.go.id/pemilih/dps/listNasional.json'
    fetch_propinsi = ['DKI JAKARTA']
    logs = {
        'info': [],
        'error': []
    }

    def add_arguments(self, parser):
        parser.add_argument('id', nargs='+', type=int)

    def store_kecamatan(self, kabkota_id):
        try:
            kabkota = pm.KabKota.objects.get(id=kabkota_id)
        except:
            print('KabKota dengan id={} tidak ditemukan'.format(kabkota_id))
            exit(1)

        self.start_url = "https://pilkada2017.kpu.go.id/pemilih/dps/{}/{}/listDps.json".format(kabkota.propinsi.namaWilayah, kabkota.namaKabKota)

        try:
            r = requests.get(self.start_url)
            d = json.loads(r.text)
            list_kecamatan = d['aaData']
        except Exception as e:
            print(e)
            print('Failed when storing a kecamatan -- url="{}"'.format(self.start_url))
            exit()

        for item in list_kecamatan:
            kecamatan, created = pm.Kecamatan.objects.get_or_create(
                namaKecamatan=item['namaKecamatan'],
                propinsi=kabkota.propinsi,
                kabkota=kabkota,

                jmlTps=item['jmlTps'],

                jmlPemilihLaki=item['jmlPemilihLaki'],
                jmlPemilihPerempuan=item['jmlPemilihPerempuan'],

                jmlPemilihPemulaLaki=item['jmlPemilihPemulaLaki'],
                jmlPemilihPemulaPerempuan=item['jmlPemilihPemulaPerempuan'],

                jmlPemilihKosong=item['jmlPemilihKosong'],

                jmlDifabel1=item['jmlDifabel1'],
                jmlDifabel2=item['jmlDifabel2'],
                jmlDifabel3=item['jmlDifabel3'],
                jmlDifabel4=item['jmlDifabel4'],
                jmlDifabel5=item['jmlDifabel5'],

                totalPemilihPemula=item['totalPemilihPemula'],
                totalDifabel=item['totalDifabel'],
                totalPemilih=item['totalPemilih'],

                persenPemilihPemula=item['persenPemilihPemula'],
                persenDifabel=item['persenDifabel'],
            )
            if created:
                info = 'Menambahakan kecamatan "{}"'.format(item['namaKecamatan'])
            else:
                info = 'Data sudah ada: kecamatan "{}"'.format(item['namaKecamatan'])
            print(info)
            self.logs['info'].append(info)

    def handle(self, *args, **options):
        self.store_kecamatan(options['id'][0])

