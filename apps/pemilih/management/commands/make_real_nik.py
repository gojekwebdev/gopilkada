from django.core.management.base import BaseCommand
from pemilih import models as pm
import re


class Command(BaseCommand):
    help = 'Fetch JSON from KPU.GO.ID for Pilkada 2017 -- (Data Pemiih & TPS)'
    start_url = 'https://pilkada2017.kpu.go.id/pemilih/dps/listNasional.json'
    fetch_propinsi = ['DKI JAKARTA']

    def add_arguments(self, parser):
        parser.add_argument('offset_start', nargs='+', type=int)
        parser.add_argument('n_data', nargs='+', type=int)

    def empty_tangal_lahir(self):
        return pm.Pemilih.objects.filter(tanggalLahir__isnull=True)

    def make_real_nik(self, offset, n_data):
        """
        format nik (secara terurut dari depan ke belakang)
          - 2 digit : kode propinsi
          - 2 digit : kode kota/kabupaten
          - 2 digit : kode kecamatan
          - 2 digit : tanggal lahir, khusus perempuan di tambahkan 40
          - 2 digit : bulan lahir
          - 2 digit : tahun lahir (2 digit terakhir dari angka tahun)
          - 4 digit : id/counter/urutan unik dengan referensi digit-sigit sebelumnya
        """
        list_pemilih = pm.Pemilih.objects.filter(nik_lengkap__isnull=True)[offset:(offset + n_data)]
        for idx, item in enumerate(list_pemilih):
            m = re.match("(^\d{6})\W+(\d{4}$)", item.nik)
            if not m or len(m.groups()) != 2:
                print("pos[{}:{}] -- id:{} nik:{} error".format((offset + idx), (offset + n_data), item.id, item.nik))

                # set malformed level
                m = re.match("(^\d{6})\*{6}(\d{3})\*", item.nik)
                if m and len(m.groups()) == 2:
                    item.nik_malformed_level = 1
                    item.save()
                    continue

                m = re.match("(^\d{6})\*{10}", item.nik)
                if m and len(m.groups()) == 1:
                    item.nik_malformed_level = 2
                    item.save()
                    continue

                m = re.match("(^\d{4})\*{12}", item.nik)
                if m and len(m.groups()) == 1:
                    item.nik_malformed_level = 3
                    item.save()
                    continue

                m = re.match("(^\d{1})\*{15}", item.nik)
                if m and len(m.groups()) == 1:
                    item.nik_malformed_level = 4
                    item.save()
                    continue

                continue
            nik_mid = int(item.tanggalLahir.strftime("%d%m%y"))
            if item.jenisKelamin == 2:
                nik_mid = nik_mid + 400000

            item.nik_lengkap = "{}{:06d}{}".format(m.groups()[0], nik_mid, m.groups()[1])
            item.save()
            print("pos[{}:{}] -- id:{} nik:{} tl:{} nik_lengkap:{}".format((offset + idx), (offset + n_data), item.id, item.nik, item.tanggalLahir.strftime('%d%m%y'), item.nik_lengkap))

    def handle(self, *args, **options):
        empty_tangal_lahir = self.empty_tangal_lahir()
        if empty_tangal_lahir.count() > 0:
            print('Terdapat data pemilih dengan tanggalLahir yang kosong')
            print(empty_tangal_lahir)
            exit()
        self.make_real_nik(options['offset_start'][0], options['n_data'][0])
