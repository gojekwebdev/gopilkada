from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from tpspilkada2017 import settings
from pemilih import models as pm
import os
import re
import datetime
import requests
import json
import pprint


class Command(BaseCommand):
    help = "Fetch JSON from KPU.GO.ID for Pilkada 2017 -- (Data Pemiih & TPS)\
        Available Sub-Commands: [store_propinsi, store_kabkota, store_kecamatan, store_kelurahan, store_tps, store_pemilih]"
    start_url = 'https://pilkada2017.kpu.go.id/pemilih/dps/listNasional.json'
    fetch_propinsi = ['DKI JAKARTA']

    def add_arguments(self, parser):
        parser.add_argument('offset', nargs='+', type=int)
        parser.add_argument('ammount', nargs='+', type=int)

    def import_pemilih_json(self, offset=0, ammount=1000000):
        JSON_ROOT_DIR = os.path.join(settings.MEDIA_ROOT, 'pemilih')

        tps_list = pm.Tps.objects.filter(propinsi__namaWilayah__in=self.fetch_propinsi)[offset:(offset + ammount)]
        n_tps = tps_list.count()

        for t in range(n_tps):
            tps = tps_list[t]
            # read json file content
            json_file = os.path.join(JSON_ROOT_DIR, str(tps.propinsi.id), str(tps.kabkota.id), str(tps.kecamatan.id), str(tps.kelurahan.id), "{}.json".format(tps.no))
            if not os.path.exists(json_file):
                print("File not exists! {}".format(json_file))
                continue
            with open(json_file, 'r') as fd:
                content = fd.read()
                d = json.loads(content)
                n_pemilih = len(d['data'])
                for i in range(n_pemilih):
                    item = d['data'][i]
                    tanggalLahir = datetime.datetime.strptime(item['tanggalLahir'], '%b %d, %Y %I:%M:%S %p').date()
                    if item['jenisKelamin'] == 'Laki-Laki':
                        jenisKelamin = 1
                    else:
                        jenisKelamin = 2
                    try:
                        tempatLahir = item['tempatLahir']
                    except:
                        tempatLahir = None
                    pemilih, created = pm.Pemilih.objects.get_or_create(
                        nama=item['nama'],
                        id_pemilih=item['id'],
                        nik=item['nik'],
                        ektp=item['ektp'],

                        tps=tps,

                        propinsi=tps.propinsi,
                        kabkota=tps.kabkota,
                        kecamatan=tps.kecamatan,
                        kelurahan=tps.kelurahan,
                        idWilayah=item['idWilayah'],

                        tempatLahir=tempatLahir,
                        tanggalLahir=tanggalLahir,
                        jenisKelamin=jenisKelamin,
                        cacat=item['cacat'],
                        kawin=item['kawin'],
                    )
                    if created:
                        info = 'kabkota:{}, kecamatan:{}, kelurahan:{} tps:[{}:{}--{}--{}:{}] pemilih[{}:{}] Menambahakan pemilih "{}"'.format(tps.kabkota.namaKabKota, tps.kecamatan.namaKecamatan, tps.kelurahan.namaKelurahan, t, n_tps, tps.no, (t + offset), (n_tps + offset), i, n_pemilih, item['nama'])
                    else:
                        info = 'kabkota:{}, kecamatan:{}, kelurahan:{} tps:[{}:{}--{}--{}:{}] pemilih[{}:{}] Data sudah ada: pemilih "{}"'.format(tps.kabkota.namaKabKota, tps.kecamatan.namaKecamatan, tps.kelurahan.namaKelurahan, t, n_tps, tps.no, (t + offset), (n_tps + offset), i, n_pemilih, item['nama'])
                    print(info)

    def handle(self, *args, **options):
        self.import_pemilih_json(options['offset'][0], options['ammount'][0])

