from django.core.management.base import BaseCommand
from pemilih import models as pm
import datetime
import requests
import json
import os
import subprocess


class Command(BaseCommand):
    help = 'Fetch JSON from KPU.GO.ID for Pilkada 2017 -- (Data Pemiih & TPS)'
    start_url = 'https://pilkada2017.kpu.go.id/pemilih/dps/listNasional.json'
    fetch_propinsi = ['DKI JAKARTA']
    logs = {
        'info': [],
        'error': []
    }

    MANAGEMENT_CMD = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))), 'manage.py')

    def add_arguments(self, parser):
        parser.add_argument('offset_start', nargs='+', type=int)
        parser.add_argument('n_tps', nargs='+', type=int)

    def store_pemilih(self, offset, limit):
        tps_list = pm.Tps.objects.filter(propinsi__namaWilayah__in=self.fetch_propinsi)[offset:(offset + limit)]
        n_tps = tps_list.count()
        for i in range(n_tps):
            subprocess.Popen([self.MANAGEMENT_CMD, 'fetch_pemilih', "{}".format(tps_list[i].id)])

    def handle(self, *args, **options):
        self.store_pemilih(options['offset_start'][0], options['n_tps'][0])
