from django.db import models


NIK_MALFORMED_LEVEL = (
    (1, '123456******123*'),
    (2, '123456**********'),
    (2, '1234************'),
    (3, '1***************'),
)


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, verbose_name='dibuat pada')
    updated = models.DateTimeField(auto_now=True, verbose_name='diubah pada')

    class Meta:
        abstract = True


class StatAreaModel(models.Model):
    jmlTps = models.PositiveIntegerField(verbose_name='jumlah TPS')

    jmlPemilihLaki = models.PositiveIntegerField()
    jmlPemilihPerempuan = models.PositiveIntegerField()

    jmlPemilihPemulaLaki = models.PositiveIntegerField()
    jmlPemilihPemulaPerempuan = models.PositiveIntegerField()

    jmlPemilihKosong = models.PositiveIntegerField()

    jmlDifabel1 = models.PositiveIntegerField(default=0)
    jmlDifabel2 = models.PositiveIntegerField(default=0)
    jmlDifabel3 = models.PositiveIntegerField(default=0)
    jmlDifabel4 = models.PositiveIntegerField(default=0)
    jmlDifabel5 = models.PositiveIntegerField(default=0)

    totalPemilihPemula = models.PositiveIntegerField()
    totalDifabel = models.PositiveIntegerField(default=0)
    totalPemilih = models.PositiveIntegerField(default=0)

    persenPemilihPemula = models.FloatField()
    persenDifabel = models.FloatField()

    class Meta:
        abstract = True


class Propinsi(BaseModel, StatAreaModel):
    namaWilayah = models.CharField(max_length=255, unique=True, verbose_name='nama')

    class Meta:
        ordering = ['namaWilayah']
        verbose_name_plural = "Propinsi"

    def __str__(self):
        return self.namaWilayah


class KabKota(BaseModel, StatAreaModel):
    namaKabKota = models.CharField(max_length=255, verbose_name='nama', db_index=True)
    propinsi = models.ForeignKey(Propinsi)

    class Meta:
        ordering = ['namaKabKota']
        verbose_name = "kab/kota"
        verbose_name_plural = "kab/kota"

    def __str__(self):
        return self.namaKabKota


class Kecamatan(BaseModel, StatAreaModel):
    namaKecamatan = models.CharField(max_length=255, verbose_name='nama', db_index=True)
    propinsi = models.ForeignKey(Propinsi)
    kabkota = models.ForeignKey(KabKota)

    class Meta:
        ordering = ['namaKecamatan']
        verbose_name_plural = "kecamatan"

    def __str__(self):
        return self.namaKecamatan


class Kelurahan(BaseModel, StatAreaModel):
    namaKelurahan = models.CharField(max_length=255, verbose_name='nama', db_index=True)
    propinsi = models.ForeignKey(Propinsi)
    kabkota = models.ForeignKey(KabKota)
    kecamatan = models.ForeignKey(Kecamatan)

    class Meta:
        ordering = ['namaKelurahan']
        verbose_name_plural = "kelurahan"

    def __str__(self):
        return self.namaKelurahan


class Tps(BaseModel):
    no = models.PositiveIntegerField(db_index=True)
    propinsi = models.ForeignKey(Propinsi)
    kabkota = models.ForeignKey(KabKota)
    kecamatan = models.ForeignKey(Kecamatan)
    kelurahan = models.ForeignKey(Kelurahan)

    jmlPemilihLaki = models.PositiveIntegerField()
    jmlPemilihPerempuan = models.PositiveIntegerField()

    jmlPemilihPemulaLaki = models.PositiveIntegerField()
    jmlPemilihPemulaPerempuan = models.PositiveIntegerField()

    jmlPemilihKosong = models.PositiveIntegerField()

    jmlDifabel1 = models.PositiveIntegerField(default=0)
    jmlDifabel2 = models.PositiveIntegerField(default=0)
    jmlDifabel3 = models.PositiveIntegerField(default=0)
    jmlDifabel4 = models.PositiveIntegerField(default=0)
    jmlDifabel5 = models.PositiveIntegerField(default=0)

    totalPemilihPemula = models.PositiveIntegerField()
    totalDifabel = models.PositiveIntegerField(default=0)
    totalPemilih = models.PositiveIntegerField(default=0)

    persenPemilihPemula = models.FloatField()
    persenDifabel = models.FloatField()

    class Meta:
        ordering = ['no']
        verbose_name_plural = "tps"

    def __str__(self):
        return str(self.no)


class Pemilih(BaseModel):
    nama = models.CharField(max_length=255, db_index=True)
    id_pemilih = models.PositiveIntegerField()
    nik = models.CharField(max_length=50, db_index=True)
    nik_lengkap = models.CharField(max_length=50, db_index=True, blank=True, null=True)
    nik_malformed_level = models.PositiveSmallIntegerField(choices=NIK_MALFORMED_LEVEL, blank=True, null=True)
    ektp = models.PositiveSmallIntegerField(default=1)

    tps = models.ForeignKey(Tps)

    propinsi = models.ForeignKey(Propinsi)
    kabkota = models.ForeignKey(KabKota, verbose_name='Kabupaten/Kota')
    kecamatan = models.ForeignKey(Kecamatan)
    kelurahan = models.ForeignKey(Kelurahan)
    idWilayah = models.PositiveIntegerField()

    tempatLahir = models.CharField(max_length=255, verbose_name='tempat lahir', blank=True, null=True)
    tanggalLahir = models.DateField(verbose_name='tanggal lahir')
    jenisKelamin = models.PositiveSmallIntegerField(choices=((1, 'Laki-Laki'), (2, 'Perempuan'), (3, '-')), verbose_name='jenis kelamin')

    cacat = models.PositiveSmallIntegerField()
    kawin = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name_plural = "pemilih"

    def __str__(self):
        return self.nama

    @property
    def tps_no(self):
        return self.tps.no
