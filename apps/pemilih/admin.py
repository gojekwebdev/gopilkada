from django.contrib import admin

from . import models


class PropinsiAdmin(admin.ModelAdmin):
    list_display = ['namaWilayah', 'jmlTps', 'totalPemilih']


class PemilihAdmin(admin.ModelAdmin):
    search_fields = ['nama']
    list_display = ['nama', 'nik', 'jenisKelamin', 'tps', 'kelurahan', 'kecamatan', 'kabkota', 'propinsi']


class TpsAdmin(admin.ModelAdmin):
    searh_fields = ['kelurahan__namaKelurahan', 'kecamatan__namaKecamatan', 'kabkota__namaKabKota', 'propinsi__namaWilayah']
    list_display = ['no', 'totalPemilih', 'kelurahan', 'kecamatan', 'kabkota', 'propinsi']


admin.site.register(models.Propinsi, PropinsiAdmin)
admin.site.register(models.KabKota)
admin.site.register(models.Kecamatan)
admin.site.register(models.Kelurahan)
admin.site.register(models.Tps, TpsAdmin)
admin.site.register(models.Pemilih, PemilihAdmin)
